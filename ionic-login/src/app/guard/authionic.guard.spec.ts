import { TestBed, async, inject } from '@angular/core/testing';

import { AuthionicGuard } from './authionic.guard';

describe('AuthionicGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthionicGuard]
    });
  });

  it('should ...', inject([AuthionicGuard], (guard: AuthionicGuard) => {
    expect(guard).toBeTruthy();
  }));
});
